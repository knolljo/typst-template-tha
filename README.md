# Typst Template THA

This is a `typst` template for writing papers at
[Technische Hochschule Augsburg](https://tha.de).

## Installation

- [typst](https://github.com/typst/typst?tab=readme-ov-file#installation): Typst compiler
- [just](https://just.systems/man/en/chapter_4.html): Command runner
- **template**: `git clone https://github.com/knolljo/typst-template-tha`: Template Repository

## Usage

- **Writing**: Write using the `typst` markup language
- **Bibliograhpy**: Put BibTex Citations into `src/sources.bib`
- **Building**: Run `just compile`, to build a pdf to `out.pdf`

```sh
just compile  # For compiling the pdf 
just watch  # When developing for live preview
```
