# A justfile for running thist typst template project
export TYPST_ROOT := "."
export TYPST_FONT_PATHS := "./assets/fonts/"

alias c := compile
alias w := watch

infile := "src/index.typ"
outfile := "out.pdf"

# Compile the source files to out.pdf
compile:
    typst compile {{infile}} {{outfile}}

# Recompile on file changes while editing
watch:
    typst watch {{infile}} {{outfile}}

# List available fonts
fonts:
    @typst fonts
