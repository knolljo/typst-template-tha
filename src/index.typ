#import "_preamble.typ": *

#show: doc => conf(
  "<Typst Vorlage>", // Main title of the document
  "<SoSe24>", // Subtitle
  // A list of authors
  // Required fields are name and email, everything else gets appended
  authors: (
    (
      name: "Johannes Knoll", email: "johannes.knoll@tha.de", major: "Informatik",
    ),
  ), doc,
)

= Einführung

== Problem
== Motivation
== Ziele
== Überblick

= Verwandte Arbeiten

@test // A citation

= Fazit / Resümee

