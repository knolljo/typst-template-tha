// Font configuration
#let font = (
  heading: "Latin Modern Sans",
  normal: "Latin Modern Roman",
  size: (
    normal: 10pt,
    sub: 12pt,
    subsub: 11pt,
    big: 14pt,
  )
)
#let lang = "de"

#let tha(term, color: rgb("#ff0350")) = {
  text(color, box[ THA #term])
}

// Build the frontpage
#let front(title, subtitle, authors) = {
  set page(margin: (top: 35mm), header: none, footer: none)

  // Title and subtitle with logo
  figure(
    image("../assets/images/THA_Logo.png", width: 40%, fit: "contain")
  )
  text(font: font.heading)[
      #align(center)[
        #text(20pt, weight: "bold", title) \
        #text(12pt, weight: "semibold", subtitle)
    ]
  ]

  // Grid of authors
  grid(
    columns: (1fr,) * authors.len(),
    ..authors.map(author => {
      align(center)[
        #author.remove("name") \
        #link("mailto:" + author.remove("email")) \
        #for val in author.values() {
          text(val)
          linebreak()
        }
      ]
    })
  )

  // Date of compilation
  align(center)[
    #block(
      datetime.today().display("[day].[month].[year]")
    )
  ]

  outline(
    title: {
      text(font: font.heading, size: font.size.big, weight: 600, "Inhaltsangabe")
      v(0.15em)
    },
    indent: true,
    depth: 4
  )
}

#let conf(
  title,
  subtitle,
  authors: (),
  doc,
) = {  
  set heading(numbering: "1.1")
  set text(
    lang: lang,
    font: font.normal,
  )
  set document(
    title: title,
    author: authors.first().name,
    date: datetime.today(),
  )
  set page(
    numbering: "1",
    header: [
      #block(below: .4em)[
        #title
        #h(1fr)
        #subtitle
      ]
      #line(length: 100%, stroke: 0.5pt)
    ],
    footer: [
      #place(left, dy: -1mm)[#image("../assets/images/THA_Logo.png", width: 15%)]
      #h(1fr)
      #counter(page).display("1")
    ],
  )
  
  show link: set text(fill: blue.darken(60%))
  show ref: set text(fill: blue.darken(60%))
  show figure: set text(size: 0.85em)
  show heading: set text(font: font.heading, weight: "bold")
  show heading: set block(below: 1.85em, above: 1.75em)
  show heading.where(level: 2): set text(size: font.size.sub)
  show heading.where(level: 3): set text(size: font.size.subsub)
  show outline.entry.where(
    level: 1
  ): it => {
    text(font:font.heading)[#strong(it)]
  }

  // Insert front page
  front(title, subtitle, authors)
  pagebreak()
  doc 
  bibliography(title: [Literaturverzeichnis], "sources.bib")
}
